<?php

return [
    'plugin' => [
        'name' => 'دیدگاه ها',
        'description' => 'Allows users comment content',
    ],

    'comment' => [
        'author' => 'نویسنده',
        'content' => 'محتوا',
        'status' => 'وضعیت',
        'comments' => 'دیدگاه ها',
    ],
    'settings' => [
        'allow_guest_label' => 'دیدگاه مهمان',
        'allow_guest_above' => 'به مهمان اجازه دیدگاه داده شود',
        'status_label' => 'وضعیت',
        'status_above' => 'وضعیت اولیه برای دیدگاه کاربر',
        'section_recaptcha_label' => 'reCAPTCHA تنظیمات',
        'section_recaptcha_comments' => 'Show or Hide reCAPTCHA on contact us form',
        'recaptcha_label' => 'reCAPTCHA',
        'recaptcha_comment' => 'Display reCAPTCHA widget on the form',
        'site_key_label' => 'Site Key',
        'site_key_comment' => 'Your site key provided by google',
        'secret_key_label' => 'Secret Key',
        'secret_key_comment' => 'Your secret key provided by google'
    ]
];
